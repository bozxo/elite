use std::alloc::System;
use rusqlite::config::DbConfig::SQLITE_DBCONFIG_ENABLE_FKEY;
use rusqlite::{self, params, Connection, Error, OptionalExtension, Transaction};
use std::path::Path;
use std::time::{SystemTime, UNIX_EPOCH};
use uuid::Uuid;

/// 打开一个数据库（开启外键）。
///
/// 若此文件不存在，则创建。
pub fn open(path: &Path) -> Result<Connection, Error> {
    let conn = Connection::open(path)?;
    conn.execute_batch(
        "BEGIN;
        CREATE TABLE IF NOT EXISTS tags (
            id TEXT PRIMARY KEY,
            name TEXT NOT NULL UNIQUE,
            fav INTEGER NOT NULL CHECK (fav in (0, 1)) DEFAULT 0
        );
        CREATE TABLE IF NOT EXISTS publishers (
            id TEXT PRIMARY KEY,
            name TEXT NOT NULL UNIQUE,
            impact REAL CHECK (impact >= 0),
            description TEXT,
            fav INTEGER NOT NULL CHECK (fav in (0, 1)) DEFAULT 0
        );
        CREATE TABLE IF NOT EXISTS authors (
            id TEXT PRIMARY KEY,
            name TEXT NOT NULL UNIQUE,
            contact TEXT,
            description TEXT,
            fav INTEGER NOT NULL CHECK (fav in (0, 1)) DEFAULT 0
        );
        CREATE TABLE IF NOT EXISTS papers (
            id TEXT PRIMARY KEY,
            name TEXT NOT NULL UNIQUE,
            publisher_id TEXT REFERENCES publishers(id),
            publish_date INTEGER,
            description TEXT,
            ctime INTEGER NOT NULL,
            mtime INTEGER NOT NULL,
            star INTEGER CHECK (star BETWEEN 0 AND 5) DEFAULT 0,
            fav INTEGER NOT NULL CHECK (fav IN (0, 1)) DEFAULT 0,
            CHECK (mtime >= ctime)
        );
        CREATE TABLE IF NOT EXISTS author_tag (
            author_id TEXT NOT NULL REFERENCES authors(id) ON DELETE CASCADE ON UPDATE CASCADE,
            tag_id TEXT NOT NULL REFERENCES tags(id) ON DELETE CASCADE ON UPDATE CASCADE,
            ctime INTEGER NOT NULL,
            PRIMARY KEY (author_id, tag_id)
        );
        CREATE TABLE IF NOT EXISTS publisher_tag (
            publisher_id TEXT NOT NULL REFERENCES publishers(id) ON DELETE CASCADE ON UPDATE CASCADE,
            tag_id TEXT NOT NULL REFERENCES tags(id) ON DELETE CASCADE ON UPDATE CASCADE,
            ctime INTEGER NOT NULL,
            PRIMARY KEY (publisher_id, tag_id)
        );
        CREATE TABLE IF NOT EXISTS paper_tag (
            paper_id TEXT NOT NULL REFERENCES papers(id) ON DELETE CASCADE ON UPDATE CASCADE,
            tag_id TEXT NOT NULL REFERENCES tags(id) ON DELETE CASCADE ON UPDATE CASCADE,
            ctime INTEGER NOT NULL,
            PRIMARY KEY (paper_id, tag_id)
        );
        CREATE TABLE IF NOT EXISTS paper_author (
            paper_id TEXT NOT NULL REFERENCES papers(id) ON DELETE CASCADE ON UPDATE CASCADE,
            author_id TEXT NOT NULL REFERENCES authors(id) ON DELETE CASCADE ON UPDATE CASCADE,
            ctime INTEGER NOT NULL,
            PRIMARY KEY (paper_id, author_id)
        );
        COMMIT;",
    )?;
    conn.set_db_config(SQLITE_DBCONFIG_ENABLE_FKEY, true)?;
    Ok(conn)
}

/// 将给定的字符串转换为SQLite LIKE 子句中的pattern。
fn pat(s: &str) -> String {
    let mut s = s
        .to_owned()
        .replace("\\", "\\\\")
        .replace("_", "\\_")
        .replace("%", "\\%");
    s.insert(0, '%');
    s.push('%');
    s
}

/// 添加一个标签，在事务上操作（不提交）。
pub fn add_tag_(tx: &Transaction, name: &str, fav: bool) -> Result<(), Error> {
    let id: String = Uuid::new_v4().to_string();
    let fav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "INSERT INTO tags (id, name, fav) VALUES (?, ?, ?)",
        params![id, name, fav],
    )?;
    Ok(())
}

/// 添加一个标签，在连接上操作（提交）。
pub fn add_tag(conn: &Connection, name: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_tag_(&tx, name, fav)?;
    tx.commit()
}

/// 按标识删标签，在事务上操作（不提交）。
fn _rm_tag_(tx: &Transaction, id: &str) -> Result<(), Error> {
    tx.execute("DELETE FROM tags WHERE id = ?", [id])?;
    Ok(())
}

/// 按标识删标签，在连接上操作（提交）。
fn _rm_tag(conn: &Connection, id: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _rm_tag_(&tx, id)?;
    tx.commit()
}

/// 按名字删标签，在事务上操作（不提交）。
pub fn rm_tag_(tx: &Transaction, name: &str) -> Result<(), Error> {
    tx.execute("DELETE FROM tags WHERE name = ?", params![name])?;
    Ok(())
}

/// 根据名字删除标签，在连接上操作（提交）。
pub fn rm_tag(conn: &Connection, name: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_tag_(&tx, name)?;
    tx.commit()
}

/// 根据收藏状态删除标签，在事务上操作（不提交）。
pub fn rm_tag_with_fav_(tx: &Transaction, fav: bool) -> Result<(), Error> {
    let fav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute("DELETE FROM tags WHERE fav = ?", params![fav])?;
    Ok(())
}

/// 根据收藏状态删除标签，在连接上操作（提交）。
pub fn rm_tag_with_fav(conn: &Connection, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_tag_with_fav_(&tx, fav)?;
    tx.commit()
}

/// 清除所有标签，在事务上操作（不提交）。
pub fn clear_tags_(tx: &Transaction) -> Result<(), Error> {
    tx.execute("DELETE FROM tags", [])?;
    Ok(())
}

/// 清除所有标签，在连接上操作（提交）。
pub fn clear_tags(conn: &Connection) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    clear_tags_(&tx)?;
    tx.commit()
}

/// 重命名标签，在事务上操作（不提交）。
pub fn rename_tag_(tx: &Transaction, old: &str, new: &str) -> Result<(), Error> {
    tx.execute("UPDATE tags SET name = ? WHERE name = ?", [new, old])?;
    Ok(())
}

/// 重命名标签，在连接上操作（提交）。
pub fn rename_tag(conn: &Connection, old: &str, new: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rename_tag_(&tx, old, new)?;
    tx.commit()
}

/// 为标识所指的标签设置收藏状态，在事务上操作（不提交）。
fn _set_tag_fav_(tx: &Transaction, id: &str, fav: bool) -> Result<(), Error> {
    let fav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute("UPDATE tags SET fav = ? WHERE id = ?", params![fav, id])?;
    Ok(())
}

/// 为标识所指的标签设置收藏状态，在连接上操作（提交）。
fn _set_tag_fav(conn: &Connection, id: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_tag_fav_(&tx, id, fav)?;
    tx.commit()
}

/// 为名字所指的标签设置收藏状态，在事务上操作（不提交）。
pub fn set_tag_fav_(tx: &Transaction, name: &str, fav: bool) -> Result<(), Error> {
    let fav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute("UPDATE tags SET fav = ? WHERE name = ?", params![fav, name])?;
    Ok(())
}

/// 为名字所指的标签设置收藏状态，在连接上操作（不提交）。
pub fn set_tag_fav(conn: &Connection, name: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_tag_fav_(&tx, name, fav)?;
    tx.commit()
}

/// 查询指定标签标识是否存在，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
fn _exists_tag_(tx: &Transaction, id: &str) -> Result<bool, Error> {
    let mut stmt = tx.prepare("SELECT * FROM tags WHERE id = ?")?;
    stmt.exists([id])
}

/// 检查所给标签标识是否存在，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
fn _exists_tag(conn: &Connection, id: &str) -> Result<bool, Error> {
    let tx = conn.unchecked_transaction()?;
    _exists_tag_(&tx, id)
}

/// 查询给定标签名是否存在，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
pub fn exists_tag_(tx: &Transaction, name: &str) -> Result<bool, Error> {
    let mut stmt = tx.prepare("SELECT * FROM tags WHERE name = ?")?;
    stmt.exists([name])
}

/// 查询给定标签名是否存在，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
pub fn exists_tag(conn: &Connection, name: &str) -> Result<bool, Error> {
    let tx = conn.unchecked_transaction()?;
    exists_tag_(&tx, name)
}

/// 按标识取标签，在事务上操作（不提交）。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
fn _get_tag_(tx: &Transaction, id: &str) -> Result<Option<(String, String, bool)>, Error> {
    tx.query_row("SELECT name, fav FROM tags WHERE id = ?", [id], |row| {
        Ok((
            id.to_owned(),
            row.get(0)?,
            match row.get(1)? {
                0 => false,
                _ => true,
            },
        ))
    })
    .optional()
}

/// 根据标识获取标签，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
fn _get_tag(conn: &Connection, id: &str) -> Result<Option<(String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    _get_tag_(&tx, id)
}

/// 按名字获取标签，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tag_(tx: &Transaction, name: &str) -> Result<Option<(String, String, bool)>, Error> {
    tx.query_row(
        "SELECT id, fav FROM tags WHERE id = ?",
        params![name],
        |row| {
            Ok((
                row.get(0)?,
                name.to_owned(),
                match row.get(1)? {
                    0 => false,
                    _ => true,
                },
            ))
        },
    )
    .optional()
}

/// 按名取标签，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tag(conn: &Connection, name: &str) -> Result<Option<(String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tag_(&tx, name)
}

/// 按收藏状态取标签，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tags_with_fav_(
    tx: &Transaction,
    fav: bool,
) -> Result<Vec<(String, String, bool)>, Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    let mut stmt = tx.prepare("SELECT id, name FROM tags WHERE fav = ?")?;

    let rows = stmt.query_map([ifav], |row| {
        Ok((row.get::<_, String>(0)?, row.get::<_, String>(1)?))
    })?;
    let mut tags: Vec<(String, String, bool)> = Vec::new();
    for row in rows {
        let (id, name) = row?;
        tags.push((id, name, fav));
    }
    Ok(tags)
}

/// 获取处于指定收藏状态的标签，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tags_with_fav(
    conn: &Connection,
    fav: bool,
) -> Result<Vec<(String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tags_with_fav_(&tx, fav)
}

/// 按部分名获取标签，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tags_with_name_part_(
    tx: &Transaction,
    name_part: &str,
) -> Result<Vec<(String, String, bool)>, Error> {
    // 由于SQLite视字符 '_' 和 '%' 为通配符，故须转义，SQLite中的默认转义字符为 '\'。
    // 又因 Linux 平台下文件名中可以包含字符 '\'，故也需对用户可能输入的 '\' 字符进行转义处理。
    let name_pat = pat(name_part);

    let mut stmt = tx.prepare("SELECT id FROM tags WHERE name LIKE ?")?;
    let rows = stmt.query_map([name_pat], |row| Ok(row.get(0)?))?;

    let mut tags = Vec::new();
    for row in rows {
        let id: String = row?;
        let tag = _get_tag_(tx, &id)?.unwrap();
        tags.push(tag);
    }

    Ok(tags)
}

/// 根据部分名获取标签，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tags_with_name_part(
    conn: &Connection,
    name_part: &str,
) -> Result<Vec<(String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tags_with_name_part_(&tx, name_part)
}

/// 获取所有标签，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tags_(tx: &Transaction) -> Result<Vec<(String, String, bool)>, Error> {
    let mut stmt = tx.prepare("SELECT id, name, fav FROM tags")?;
    let rows = stmt.query_map([], |row| {
        Ok((
            row.get::<_, String>(0)?,
            row.get::<_, String>(1)?,
            match row.get::<_, i32>(2)? {
                0 => false,
                _ => true,
            },
        ))
    })?;
    let mut tags: Vec<(String, String, bool)> = Vec::new();
    for row in rows {
        tags.push(row?);
    }
    Ok(tags)
}

/// 获取所有标签，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回`(id, name, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - fav：收藏状态。
pub fn get_tags(conn: &Connection) -> Result<Vec<(String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tags_(&tx)
}

/// 添加一个作者，在事务上操作（不提交）。
///
/// 作者信息包括：名字、联系方式、描述、收藏状态。
pub fn add_author_(
    tx: &Transaction,
    name: &str,
    contact: Option<String>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let id: String = Uuid::new_v4().to_string();
    let ifav: i32 = match fav {
        false => 0,
        true => 1,
    };

    tx.execute(
        "INSERT INTO authors (id, name, contact, description, fav) VALUES (?, ?, ?, ?, ?)",
        params![id, name, contact, description, ifav],
    )?;

    Ok(())
}

/// 添加一个作者的基本信息。
///
/// 作者信息包括：名字、联系方式、描述、收藏状态。
pub fn add_author(
    conn: &Connection,
    name: &str,
    contact: Option<String>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_author_(&tx, name, contact, description, fav)?;
    tx.commit()
}

/// 按标识删作者，在事务上操作（不提交）。
fn _rm_author_(tx: &Transaction, id: &str) -> Result<(), Error> {
    tx.execute("DELETE FROM authors WHERE id = ?", [id])?;
    Ok(())
}

/// 按标识删作者，在连接上操作（提交）。
fn _rm_author(conn: &Connection, id: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _rm_author_(&tx, id)?;
    tx.commit()
}

/// 按名删作者，在事务上操作（不提交）。
pub fn rm_author_(tx: &Transaction, name: &str) -> Result<(), Error> {
    tx.execute("DELETE FROM authors WHERE name = ?", [name])?;
    Ok(())
}

/// 按名删作者，在连接上操作（提交）。
pub fn rm_author(conn: &Connection, name: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_author_(&tx, name)?;
    tx.commit()
}

/// 按收藏状态删作者，在事务上操作（不提交）。
pub fn rm_author_with_fav_(tx: &Transaction, fav: &str) -> Result<(), Error> {
    tx.execute("DELETE FROM authors WHERE fav = ?", [fav])?;
    Ok(())
}

/// 按收藏状态删作者，在连接上操作（提交）。
pub fn rm_author_with_fav(conn: &Connection, fav: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_author_with_fav_(&tx, fav)?;
    tx.commit()
}

/// 删除全部作者，在事务上操作（不提交）。
pub fn clear_authors_(tx: &Transaction) -> Result<(), Error> {
    tx.execute("DELETE FORM authors", [])?;
    Ok(())
}

/// 删除全部作者（提交）。
pub fn clear_authors(conn: &Connection) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    clear_authors_(&tx)?;
    tx.commit()
}

/// 重命名作者，在事务上操作（不提交）。
pub fn rename_author_(tx: &Transaction, old: &str, new: &str) -> Result<(), Error> {
    tx.execute("UPDATE authors SET name = ? WHERE name = ?", [new, old])?;
    Ok(())
}

/// 重命名作者，在连接上操作（提交）。
pub fn rename_author(conn: &Connection, old: &str, new: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rename_author_(&tx, old, new)?;
    tx.commit()
}

/// 通过标识设置作者联系方式，在事务上操作（不提交）。
fn _set_author_contact_(tx: &Transaction, id: &str, contact: &str) -> Result<(), Error> {
    tx.execute(
        "UPDATE authors SET contact = ? WHERE id = ?",
        params![contact, id],
    )?;
    Ok(())
}

/// 设置作者联系方式，在事务上操作（不提交）。
pub fn set_author_contact_(
    tx: &Transaction,
    name: &str,
    contact: Option<String>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE authors SET contact = ? WHERE name = ?",
        params![contact, name],
    )?;
    Ok(())
}

/// 通过标识设置作者联系方式（提交）。
fn _set_author_contact(conn: Connection, id: &str, contact: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_author_contact_(&tx, id, contact)?;
    tx.commit()
}

/// 设置作者联系方式（提交）。
pub fn set_author_contact(
    conn: &Connection,
    name: &str,
    contact: Option<String>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_author_contact_(&tx, name, contact)?;
    tx.commit()
}

/// 通过标识设置作者描述（不提交）。
fn _set_author_description_(
    tx: &Transaction,
    id: &str,
    description: Option<String>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE authors SET description = ? WHERE id = ?",
        params![description, id],
    )?;
    Ok(())
}

/// 设置作者描述（不提交）。
pub fn set_author_description_(
    tx: &Transaction,
    name: &str,
    description: Option<String>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE authors SET description = ? WHERE name = ?",
        params![description, name],
    )?;
    Ok(())
}

/// 通过标识设置作者描述，在连接上操作（提交）。
fn _set_author_description(
    conn: &Connection,
    id: &str,
    description: Option<String>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_author_description_(&tx, id, description)?;
    tx.commit()
}

/// 设置作者描述，在连接上操作（提交）。
pub fn set_author_description(
    conn: &Connection,
    name: &str,
    description: Option<String>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_author_description_(&tx, name, description)?;
    tx.commit()
}

/// 通过标识设置作者的收藏状态，在事务上操作（不提交）。
pub fn _set_author_fav_(tx: &Transaction, id: &str, fav: bool) -> Result<(), Error> {
    let fav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute("UPDATE authors SET fav = ? WHERE id = ?", params![fav, id])?;
    Ok(())
}

/// 设置作者的收藏状态，在事务上操作（不提交）。
pub fn set_author_fav_(tx: &Transaction, name: &str, fav: bool) -> Result<(), Error> {
    let fav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "UPDATE authors SET fav = ? WHERE name = ?",
        params![fav, name],
    )?;
    Ok(())
}

/// 通过标识设置作者的收藏状态，在连接上操作（提交）。
pub fn _set_author_fav(conn: &Connection, id: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_author_fav_(&tx, id, fav)?;
    tx.commit()
}

/// 设置作者的收藏状态，在连接上操作（提交）。
pub fn set_author_fav(conn: &Connection, name: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_author_fav_(&tx, name, fav)?;
    tx.commit()
}

/// 通过标识更新作者信息，在事务上操作（不提交）。
pub fn _update_author_(
    tx: &Transaction,
    id: &str,
    name: &str,
    contact: Option<String>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "UPDATE authors SET name = ?, contact = ?, description = ?, fav = ? WHERE id = ?",
        params![name, contact, description, ifav, id],
    )?;
    Ok(())
}

/// 更新作者的除标识和名字外的其他信息，在事务上操作（不提交）。
pub fn update_author_(
    tx: &Transaction,
    name: &str,
    contact: Option<String>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "UPDATE authors SET contact = ?, description = ?, fav = ? WHERE name = ?",
        params![contact, description, ifav, name],
    )?;
    Ok(())
}

/// 通过标识更新作者信息，在连接上操作（提交）。
pub fn _update_author(
    conn: &Connection,
    id: &str,
    name: &str,
    contact: Option<String>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _update_author_(&tx, id, name, contact, description, fav)?;
    tx.commit()
}

/// 更新作者除标识和名字外的其他信息，在连接上操作（提交）。
pub fn update_author(
    conn: &Connection,
    name: &str,
    contact: Option<String>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    update_author_(&tx, name, contact, description, fav)?;
    tx.commit()
}

/// 查询用户标识是否存在，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
fn exists_authorid_(tx: &Transaction, id: &str) -> Result<bool, rusqlite::Error> {
    let mut stmt = tx.prepare("SELECT * FROM authors WHERE id = ? LIMIT 1")?;
    stmt.exists([id])
}

/// 检查用户标识是否存在，在连接上操作。
///
/// 不修改数据库内数据，故无提交。
fn exists_authorid(conn: &Connection, id: &str) -> Result<bool, rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    exists_authorid_(&tx, id)
}

/// 检查用户名是否存在，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
pub fn exists_author_(tx: &Transaction, name: &str) -> Result<bool, rusqlite::Error> {
    let mut stmt = tx.prepare("SELECT * FROM authors WHERE name = ? LIMIT 1")?;
    stmt.exists([name])
}

/// 检查用户名是否存在，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
pub fn exists_author(conn: &Connection, name: &str) -> Result<bool, rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    exists_author_(&tx, name)
}

/// 根据用户标识获取作者，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
fn _get_author_(
    tx: &Transaction,
    id: &str,
) -> Result<Option<(String, String, String, String, bool)>, Error> {
    tx.query_row(
        "SELECT id, name, contact, description, fav FROM authors WHERE id = ?",
        [id],
        |row| {
            Ok((
                row.get::<_, String>(0)?,
                row.get::<_, String>(1)?,
                row.get::<_, String>(2)?,
                row.get::<_, String>(3)?,
                match row.get::<_, i32>(4)? {
                    0 => false,
                    _ => true,
                },
            ))
        },
    )
    .optional()
}

/// 根据标识获取作者，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
fn _get_author(
    conn: &Connection,
    id: &str,
) -> Result<Option<(String, String, String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    _get_author_(&tx, id)
}

/// 根据名字获取作者信息，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_author_(
    tx: &Transaction,
    name: &str,
) -> Result<Option<(String, String, String, String, bool)>, Error> {
    tx.query_row(
        "SELECT id, name, contact, description, fav FROM authors WHERE name = ?",
        [name],
        |row| {
            Ok((
                row.get::<_, String>(0)?,
                row.get::<_, String>(1)?,
                row.get::<_, String>(2)?,
                row.get::<_, String>(3)?,
                match row.get::<_, i32>(4)? {
                    0 => false,
                    _ => true,
                },
            ))
        },
    )
    .optional()
}

/// 根据名字获取作者信息，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_author(
    conn: &Connection,
    name: &str,
) -> Result<Option<(String, String, String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_author_(&tx, name)
}

/// 根据部分名查找作者，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_authors_with_name_part_(
    tx: &Transaction,
    name_part: &str,
) -> Result<Vec<(String, String, String, String, bool)>, Error> {
    let mut authors: Vec<(String, String, String, String, bool)> = Vec::new();
    let name_pat = pat(name_part);
    let mut stmt =
        tx.prepare("SELECT id, name, contact, description, fav FROM authors WHERE name LIKE ?")?;

    let author_rows = stmt.query_map([name_pat], |row| {
        Ok((
            row.get::<_, String>(0)?,
            row.get::<_, String>(1)?,
            row.get::<_, String>(2)?,
            row.get::<_, String>(3)?,
            match row.get::<_, i32>(4)? {
                0 => false,
                _ => true,
            },
        ))
    })?;
    for author_row in author_rows {
        authors.push(author_row?);
    }

    Ok(authors)
}
/// 根据部分名查找作者。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_authors_with_name_part(
    conn: &Connection,
    name_part: &str,
) -> Result<Vec<(String, String, String, String, bool)>, rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    get_authors_with_name_part_(&tx, name_part)
}

/// 根据收藏状态获取作者信息，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_authors_with_fav_(
    tx: &Transaction,
    fav: bool,
) -> Result<Vec<(String, String, String, String, bool)>, Error> {
    let ifav: i32 = match fav {
        false => 0,
        true => 1,
    };

    let mut stmt =
        tx.prepare("SELECT id, name, contact, description FROM authors WHERE fav = ?")?;
    let rows = stmt.query_map([ifav], |row| {
        Ok((
            row.get::<_, String>(0)?,
            row.get::<_, String>(1)?,
            row.get::<_, String>(2)?,
            row.get::<_, String>(3)?,
            fav,
        ))
    })?;
    let mut authors: Vec<(String, String, String, String, bool)> = Vec::new();
    for row in rows {
        authors.push(row?);
    }
    Ok(authors)
}

/// 根据收藏状态获取作者信息，在事务上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_authors_with_fav(
    conn: &Connection,
    fav: bool,
) -> Result<Vec<(String, String, String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_authors_with_fav_(&tx, fav)
}

/// 获取所有作者信息，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_authors_(
    tx: &Transaction,
) -> Result<Vec<(String, String, String, String, bool)>, Error> {
    let mut stmt = tx.prepare("SELECT id, name, contact, description, fav FROM authors")?;
    let rows = stmt.query_map([], |row| {
        Ok((
            row.get::<_, String>(0)?,
            row.get::<_, String>(1)?,
            row.get::<_, String>(2)?,
            row.get::<_, String>(3)?,
            match row.get::<_, i32>(4)? {
                0 => false,
                _ => true,
            },
        ))
    })?;
    let mut authors: Vec<(String, String, String, String, bool)> = Vec::new();
    for row in rows {
        authors.push(row?);
    }
    Ok(authors)
}

/// 获取所有作者信息，在连接上操作。
///
/// 不修改数据库内数据，故无提交操作。
///
/// 返回值`(id, name, contact, description, fav)`：
///   - id：标签标识；
///   - name：标签名；
///   - contact：联系方式；
///   - description：描述；
///   - fav：收藏状态。
pub fn get_authors(
    conn: &Connection,
) -> Result<Vec<(String, String, String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_authors_(&tx)
}

/// 添加一个出版社（期刊、会议），在事务上操作（不提交）。
///
/// 出版社信息包括：标识、名字、影响因子、描述和收藏状态。
///
/// 若出版社（期刊、会议）已存在或出现其他数据库错误，则会返回Err。
pub fn add_publisher_(
    tx: &Transaction,
    name: &str,
    impact: Option<f32>,
    description: Option<String>,
    fav: bool,
) -> Result<(), rusqlite::Error> {
    let publisherid = Uuid::new_v4().to_string();
    let ifav: i32 = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "INSERT INTO publishers VALUES (?, ?, ?, ?, ?)",
        params![publisherid, name, impact, description, ifav],
    )?;
    Ok(())
}

/// 添加一个出版社（期刊、会议），在连接上操作（提交）。
///
/// 出版社信息包括：标识、名字、影响因子、描述和收藏状态。
///
/// 若出版社（期刊、会议）已存在或出现其他数据库错误，则会返回Err。
pub fn add_publisher(
    conn: &Connection,
    name: &str,
    impact: Option<f32>,
    description: Option<String>,
    fav: bool,
) -> Result<(), rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    add_publisher_(&tx, name, impact, description, fav)?;
    tx.commit()
}

/// 按标识删出版社（期刊、会议），在事务上操作（不提交）。
fn _rm_publisher_(tx: &Transaction, id: &str) -> Result<(), rusqlite::Error> {
    tx.execute("DELETE FROM publishers WHERE id = ?", [id])?;
    Ok(())
}

/// 按标识删出版社（期刊、会议），在连接上操作（提交）。
fn _rm_publisher(conn: &Connection, id: &str) -> Result<(), rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    _rm_publisher_(&tx, id)?;
    tx.commit()
}

/// 按名删出版社（期刊、会议），在事务上操作（不提交）。
pub fn rm_publisher_(tx: &Transaction, name: &str) -> Result<(), rusqlite::Error> {
    tx.execute("DELETE FROM publishers WHERE name = ?", [name])?;
    Ok(())
}

/// 按名删出版社（期刊、会议），在连接上操作（提交）。
pub fn rm_publisher(conn: &Connection, name: &str) -> Result<(), rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    rm_publisher_(&tx, name)?;
    tx.commit()
}

/// 删除所有出版社（期刊、会议），在事务上操作（不提交）。
pub fn clear_publishers_(tx: &Transaction) -> Result<(), rusqlite::Error> {
    tx.execute("DELETE FORM publishers", [])?;
    Ok(())
}

/// 删除所有出版社（期刊、会议），在连接上操作（提交）。
pub fn clear_publishers(conn: &Connection) -> Result<(), rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    clear_publishers_(&tx)?;
    tx.commit()
}

/// 通过标识设置出版社（期刊、会议）名，在事务上操作（不提交）。
fn _set_publisher_name_(tx: &Transaction, id: &str, name: &str) -> Result<(), Error> {
    tx.execute("UPDATE publishers SET name = ? WHERE id = ?", [name, id])?;
    Ok(())
}

/// 通过标识设置出版社（期刊、会议）名，在事务上操作（不提交）。
fn _set_publisher_name(conn: &Connection, id: &str, name: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_publisher_name_(&tx, id, name)?;
    tx.commit()
}

/// 重命名出版社，在事务上操作（不提交）。
pub fn rename_publisher_(tx: &Transaction, old: &str, new: &str) -> Result<(), Error> {
    tx.execute("UPDATE publishers SET name = ? WHERE name = ?", [new, old])?;
    Ok(())
}

/// 重命名出版社，在连接上操作（提交）。
pub fn rename_publisher(conn: &Connection, old: &str, new: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rename_publisher_(&tx, old, new)?;
    tx.commit()
}

/// 通过标识设置出版社（期刊、会议）影响因子，在事务上操作（不提交）。
fn _set_publisher_impact_(tx: &Transaction, id: &str, impact: Option<f32>) -> Result<(), Error> {
    tx.execute(
        "UPDATE publishers SET impact = ? WHERE id = ?",
        params![impact, id],
    )?;
    Ok(())
}

/// 通过标识设置出版社（期刊、会议）影响因子，在事务上操作（不提交）。
fn _set_publisher_impact(conn: &Connection, id: &str, impact: Option<f32>) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_publisher_impact_(&tx, id, impact)?;
    tx.commit()
}

/// 设置出版社影响因子，在事务上操作（不提交）。
pub fn set_publisher_impact_(
    tx: &Transaction,
    name: &str,
    impact: Option<f32>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE publishers SET impact = ? WHERE name = ?",
        params![impact, name],
    )?;
    Ok(())
}

/// 设置出版社影响因子，在连接上操作（提交）。
pub fn set_publisher_impact(
    conn: &Connection,
    name: &str,
    impact: Option<f32>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_publisher_impact_(&tx, name, impact)?;
    tx.commit()
}

/// 通过标识设置出版社（期刊、会议）描述，在事务上操作（不提交）。
fn _set_publisher_description_(
    tx: &Transaction,
    id: &str,
    description: Option<String>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE publishers SET description = ? WHERE id = ?",
        params![description, id],
    )?;
    Ok(())
}

/// 通过标识设置出版社（期刊、会议）描述，在连接上操作（提交）。
fn _set_publisher_description(
    conn: &Connection,
    id: &str,
    description: Option<String>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_publisher_description_(&tx, id, description)?;
    tx.commit()
}

/// 通过名字设置出版社（期刊、会议）描述，在事务上操作（不提交）。
pub fn set_publisher_description_(
    tx: &Transaction,
    name: &str,
    description: Option<String>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE publishers SET description = ? WHERE name = ?",
        params![description, name],
    )?;
    Ok(())
}

/// 通过名字设置出版社（期刊、会议）描述，在连接上操作（提交）。
pub fn set_publisher_description(
    conn: &Connection,
    name: &str,
    description: Option<String>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_publisher_description_(&tx, name, description)?;
    tx.commit()
}

/// 通过标识设置出版社（期刊、会议）收藏状态，在事务上操作（不提交）。
fn _set_publisher_fav_(tx: &Transaction, id: &str, fav: bool) -> Result<(), Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "UPDATE publishers SET fav = ? WHERE id = ?",
        params![ifav, id],
    )?;
    Ok(())
}

/// 通过标识设置出版社（期刊、会议）收藏状态，在连接上操作（提交）。
fn _set_publisher_fav(conn: &Connection, id: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_publisher_fav_(&tx, id, fav)?;
    tx.commit()
}

/// 通过名字设置出版社（期刊、会议）收藏状态，在事务上操作（不提交）。
pub fn set_fav_state_for_publisher_(tx: &Transaction, name: &str, fav: bool) -> Result<(), Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "UPDATE publishers SET fav = ? WHERE name = ?",
        params![ifav, name],
    )?;
    Ok(())
}

/// 通过名字设置出版社（期刊、会议）收藏状态，在连接上操作（提交）。
pub fn set_fav_state_of_publisher(conn: &Connection, name: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_fav_state_for_publisher_(&tx, name, fav)?;
    tx.commit()
}

/// 通过标识更新出版社（期刊、会议）信息，在事务上操作（不提交）。
fn _update_publisher_(
    tx: &Transaction,
    id: &str,
    name: &str,
    impact: Option<f32>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "UPDATE publishers SET name = ?, impact =?, description = ?, fav = ? WHERE id = ?",
        params![name, impact, description, ifav, id],
    )?;
    Ok(())
}

/// 通过标识更新出版社（期刊、会议）信息，在事务上操作（不提交）。
fn _update_publisher(
    conn: &Connection,
    id: &str,
    name: &str,
    impact: Option<f32>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _update_publisher_(&tx, id, name, impact, description, fav)?;
    tx.commit()
}

/// 通过名字更新出版社（期刊、会议）信息，在事务上操作（不提交）。
pub fn update_publisher_(
    tx: &Transaction,
    name: &str,
    impact: Option<f32>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE publishers SET impact =?, description = ?, fav = ? WHERE name = ?",
        params![impact, description, fav, name],
    )?;
    Ok(())
}

/// 通过名字更新出版社（期刊、会议）信息，在连接上操作（提交）。
pub fn update_publisher(
    conn: &Connection,
    name: &str,
    impact: Option<f32>,
    description: Option<String>,
    fav: bool,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    update_publisher_(&tx, name, impact, description, fav)?;
    tx.commit()
}

/// 检查出版社（期刊、会议）标识是否存在，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
fn exists_publisherid_(tx: &Transaction, id: &str) -> Result<bool, rusqlite::Error> {
    let mut stmt = tx.prepare("SELECT * FROM publishers WHERE id = ?")?;
    stmt.exists([id])
}

/// 检查出版社（期刊、会议）标识是否存在，在连接上操作。
///
/// 不修改数据库内数据，故无提交。
fn exists_publisherid(conn: &Connection, id: &str) -> Result<bool, rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    exists_publisherid_(&tx, id)
}

/// 检查出版社（期刊、会议）名字是否存在，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
pub fn exists_publisher_(tx: &Transaction, name: &str) -> Result<bool, rusqlite::Error> {
    let mut stmt = tx.prepare("SELECT * FROM publishers WHERE name = ?")?;
    stmt.exists([name])
}

/// 检查出版社（期刊、会议）名字是否存在，在连接上操作。
///
/// 不修改数据库内数据，故无提交。
pub fn exists_publisher(conn: &Connection, name: &str) -> Result<bool, rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    exists_publisher_(&tx, name)
}

/// 根据标识获取出版社（期刊、会议）信息，在事务上操作。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
fn _get_publisher_(
    tx: &Transaction,
    id: &str,
) -> Result<Option<(String, String, Option<f32>, Option<String>, bool)>, Error> {
    tx.query_row(
        "SELECT name, impact, description, fav FROM publishers WHERE id = ?",
        params![id],
        |row| {
            Ok((
                id.to_owned(),
                row.get::<_, String>(0)?,
                row.get::<_, Option<f32>>(1)?,
                row.get::<_, Option<String>>(2)?,
                row.get::<_, bool>(3)?,
            ))
        },
    )
    .optional()
}

/// 根据标识获取出版社信息，在连接上操作。
///
/// 不修改数据库内数据，故无提交。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
fn _get_publisher(
    conn: &Connection,
    id: &str,
) -> Result<Option<(String, String, Option<f32>, Option<String>, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    _get_publisher_(&tx, id)
}

/// 根据名字获取出版社。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
pub fn get_publisher_(
    tx: &Transaction,
    name: &str,
) -> Result<Option<(String, String, Option<f32>, Option<String>, bool)>, Error> {
    tx.query_row(
        "SELECT id, impact, description, fav FROM publishers WHERE name = ?",
        params![name],
        |row| {
            Ok((
                row.get::<_, String>(0)?,
                name.to_owned(),
                row.get::<_, Option<f32>>(1)?,
                row.get::<_, Option<String>>(2)?,
                row.get::<_, bool>(3)?,
            ))
        },
    )
    .optional()
}

/// 根据名字获取出版社。
///
/// 不修改数据库内数据，故无提交。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
pub fn get_publisher(
    conn: &Connection,
    name: &str,
) -> Result<Option<(String, String, Option<f32>, Option<String>, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_publisher_(&tx, name)
}

/// 获取所有出版社。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
pub fn get_publishers_(
    tx: &Transaction,
) -> Result<Vec<(String, String, Option<f32>, Option<String>, bool)>, rusqlite::Error> {
    let mut publishers: Vec<(String, String, Option<f32>, Option<String>, bool)> = Vec::new();
    let mut stmt = tx.prepare("SELECT id, name, impact, description, fav FROM publishers")?;
    let rows = stmt.query_map([], |row| {
        Ok((
            row.get(0)?,
            row.get(1)?,
            row.get(2)?,
            row.get(3)?,
            row.get(4)?,
        ))
    })?;
    for row in rows {
        let (id, name, impact, description, ifav) = row?;
        let fav = match ifav {
            0i32 => false,
            _ => true,
        };
        publishers.push((id, name, impact, description, fav));
    }
    Ok(publishers)
}
/// 获取所有出版社。
///
/// 不修改数据库内数据，故无提交。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
pub fn get_publishers(
    conn: &Connection,
) -> Result<Vec<(String, String, Option<f32>, Option<String>, bool)>, rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    get_publishers_(&tx)
}

/// 按部分名查找出版社（期刊、会议）。
///
/// 不修改数据库内数据，故无需提交，仅因事务存在时无法使用连接而设。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
pub fn get_publishers_with_namepart_(
    tx: &Transaction,
    name_part: &str,
) -> Result<Vec<(String, String, Option<f32>, Option<String>, bool)>, rusqlite::Error> {
    let mut publishers: Vec<(String, String, Option<f32>, Option<String>, bool)> = Vec::new();
    let mut name_pat = pat(name_part);
    let mut stmt =
        tx.prepare("SELECT id, name, impact, description, fav FROM publishers WHERE name LIKE ?")?;
    let rows = stmt.query_map(params![name_pat], |row| {
        Ok((
            row.get(0)?,
            row.get(1)?,
            row.get(2)?,
            row.get(3)?,
            row.get(4)?,
        ))
    })?;
    for row in rows {
        let (id, name, impact, description, ifav) = row?;
        let fav = match ifav {
            0i32 => false,
            _ => true,
        };
        publishers.push((id, name, impact, description, fav));
    }
    Ok(publishers)
}

/// 按部分名查找出版社（期刊、会议）。
///
/// 不修改数据库内数据，故无提交。
///
/// 返回值`(id，name，impact，description，fav)`。
///   - `id`：标识；
///   - `name`：名字；
///   - `impact`：影响因子；
///   - `description`：描述；
///   - `fav`：收藏状态。
pub fn get_publishers_with_namepart(
    conn: &Connection,
    name_part: &str,
) -> Result<Vec<(String, String, Option<f32>, Option<String>, bool)>, rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    get_publishers_with_namepart_(&tx, name_part)
}

/// 添加文献（不提交）。
fn add_paper_(
    tx: &Transaction,
    name: &str,
    publisher: Option<String>,
    publish_date: Option<u64>,
    description: Option<String>,
    ctime: u64,
    mtime: u64,
    star: i8,
    fav: bool,
) -> Result<(), rusqlite::Error> {
    let paper_id = Uuid::new_v4().to_string();
    let publisherid = match &publisher {
        Some(publisher) => {
            if !exists_publisher_(tx, &publisher)? {
                add_publisher_(tx, &publisher, None, None, false)?;
            }
            Some(get_publisher_(tx, &publisher)?.unwrap().0)
        }
        None => None,
    };
    let ifav: i8 = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "INSERT INTO papers (id, name, publisher_id, publish_date, description, ctime, mtime, star, fav) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
        params![
            paper_id,
            name,
            publisherid,
            publish_date,
            description,
            ctime,
            mtime,
            star,
            ifav
        ])?;

    Ok(())
}

/// 添加文献（提交）。
pub fn add_paper(
    conn: &Connection,
    name: &str,
    publisher: Option<String>,
    publish_date: Option<u64>,
    description: Option<String>,
    ctime: u64,
    mtime: u64,
    star: i8,
    fav: bool,
) -> Result<(), rusqlite::Error> {
    let tx = conn.unchecked_transaction()?;
    add_paper_(
        &tx,
        name,
        publisher,
        publish_date,
        description,
        ctime,
        mtime,
        star,
        fav,
    )?;
    tx.commit()
}

/// 通过标识删除文献，在事务上操作（不提交）。
fn _rm_paper_(tx: &Transaction, id: &str) -> Result<(), Error> {
    tx.execute("DELETE FROM papers WHERE id = ?", [id])?;
    Ok(())
}

/// 通过标识删除文献，在连接上操作（提交）。
fn _rm_paper(conn: &Connection, id: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _rm_paper_(&tx, id)?;
    tx.commit()
}

/// 通过文件名删除文献，在事务上操作（不提交）。
pub fn rm_paper_(tx: &Transaction, name: &str) -> Result<(), Error> {
    tx.execute("DELETE FROM papers WHERE name = ?", [name])?;
    Ok(())
}

/// 通过文件名删除文献，在连接上操作（提交）。
pub fn rm_paper(conn: &Connection, name: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_paper_(&tx, name)?;
    tx.commit()
}

/// 删除所有文献，在事务上操作（不提交）。
pub fn clear_papers_(tx: &Transaction) -> Result<(), Error> {
    tx.execute("DELETE FROM papers", [])?;
    Ok(())
}

/// 删除所有文献，在连接上操作（提交）。
pub fn clear_papers(conn: &Connection) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    clear_tags_(&tx)?;
    Ok(())
}

/// 通过标识修改文献名，在事务上操作（不提交）。
fn _set_paper_name_(tx: &Transaction, id: &str, name: &str) -> Result<(), Error> {
    tx.execute("UPDATE papers SET name = ? WHERE id = ?", params![name, id])?;
    Ok(())
}

/// 通过标识修改文献名，在连接上操作（提交）。
fn _set_paper_name(conn: &Connection, id: &str, name: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_paper_name_(&tx, id, name)?;
    tx.commit()
}

/// 更改文献展示名，在事务上操作（不提交）。
pub fn rename_paper_(tx: &Transaction, old: &str, new: &str) -> Result<(), Error> {
    tx.execute("UPDATE papers SET name = ? WHERE name = ?", [new, old])?;
    Ok(())
}

/// 重命名文献，在连接上操作（提交）。
pub fn rename_paper(conn: &Connection, old: &str, new: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rename_paper_(&tx, old, new)?;
    tx.commit()
}

/// 通过标识修改文献的出版社（期刊、会议），在事务上操作（不提交）。
fn _set_paper_publisherid_(
    tx: &Transaction,
    id: &str,
    publisherid: Option<String>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE papers SET publisher_id = ? WHERE id = ?",
        params![publisherid, id],
    )?;
    Ok(())
}

/// 通过标识修改文献的出版社（期刊、会议），在连接上操作（提交）。
fn _set_paper_publisherid(
    conn: &Connection,
    id: &str,
    publisherid: Option<String>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_paper_publisherid_(&tx, id, publisherid)?;
    tx.commit()
}

/// 通过文献名修改文献的出版社（期刊、会议），在事务上操作（不提交）。
pub fn set_paper_publisherid_(
    tx: &Transaction,
    name: &str,
    publisherid: Option<String>,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE papers SET publisher_id = ? WHERE name = ?",
        params![publisherid, name],
    )?;
    Ok(())
}

/// 修改文献的出版社（期刊、会议），通过文件名（不提交）。
pub fn set_paper_publisherid(
    conn: &Connection,
    name: &str,
    publisherid: Option<String>,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_paper_publisherid_(&tx, name, publisherid)?;
    tx.commit()
}

/// 修改文献的发行日期，通过标识（不提交）。
fn _set_paper_publishdate_(tx: &Transaction, id: &str, publish_date: u64) -> Result<(), Error> {
    tx.execute(
        "UPDATE papers SET publish_date = ? WHERE id = ?",
        params![publish_date, id],
    )?;
    Ok(())
}

/// 修改文献的发行日期，通过标识（提交）。
fn _set_paper_publishdate(conn: &Connection, id: &str, publish_date: u64) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_paper_publishdate_(&tx, id, publish_date)?;
    tx.commit()
}

/// 通过文献名修改文献的发行日期，在事务上操作（不提交）。
pub fn set_paper_publishdate_(
    tx: &Transaction,
    name: &str,
    publish_date: u64,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE papers SET publish_date = ? WHERE name = ?",
        params![publish_date, name],
    )?;
    Ok(())
}

/// 通过文献名修改文献的发行日期，在连接上操作（提交）。
pub fn set_paper_publishdate(
    conn: &Connection,
    name: &str,
    publish_date: u64,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_paper_publishdate_(&tx, name, publish_date)?;
    tx.commit()
}

/// 通过标识修改文献描述，在事务上操作（不提交）。
fn _set_paper_description_(tx: &Transaction, id: &str, description: &str) -> Result<(), Error> {
    tx.execute(
        "UPDATE papers SET description = ? WHERE id = ?",
        [description, id],
    )?;
    Ok(())
}

/// 通过标识修改文献描述，在连接上操作（提交）。
fn _set_paper_description(conn: &Connection, id: &str, description: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_paper_description_(&tx, id, description)?;
    tx.commit()
}

/// 通过文献名修改文献的描述，在连接上操作（不提交）。
pub fn set_paper_description_(
    tx: &Transaction,
    name: &str,
    description: &str,
) -> Result<(), Error> {
    tx.execute(
        "UPDATE papers SET description = ? WHERE name = ?",
        params![description, name],
    )?;
    Ok(())
}

/// 修改文献的描述，通过文件名（提交）。
pub fn set_paper_description(
    conn: &Connection,
    name: &str,
    description: &str,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_paper_description_(&tx, name, description)?;
    tx.commit()
}

/// 通过标识修改文献的星值，在事务上操作（不提交）。
fn _set_paper_star_(tx: &Transaction, id: &str, star: i8) -> Result<(), Error> {
    tx.execute("UPDATE papers SET star = ? WHERE id = ?", params![star, id])?;
    Ok(())
}

/// 通过标识修改文献的星值，在连接上操作（提交）。
fn _set_paper_star(conn: &Connection, id: &str, star: i8) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_paper_star_(&tx, id, star)?;
    tx.commit()
}

/// 通过文献名修改文献的星值，在事务上操作（不提交）。
pub fn set_paper_star_(tx: &Transaction, name: &str, star: i8) -> Result<(), Error> {
    tx.execute(
        "UPDATE papers SET star = ? WHERE name = ?",
        params![star, name],
    )?;
    Ok(())
}

/// 通过文件名修改文献的星值，在连接上操作（提交）。
pub fn set_paper_star(conn: &Connection, name: &str, star: i8) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_paper_star_(&tx, name, star)?;
    tx.commit()
}

/// 通过标识修改文献的收藏状态，在事务上操作（不提交）。
fn _set_paper_fav_(tx: &Transaction, id: &str, fav: bool) -> Result<(), Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute("UPDATE papers SET fav = ? WHERE id = ?", params![ifav, id])?;
    Ok(())
}

/// 通过标识修改文献的收藏状态，在连接上操作（提交）。
fn _set_paper_fav(conn: &Connection, id: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    _set_paper_fav_(&tx, id, fav)?;
    tx.commit()
}

/// 通过文件名修改文献的收藏状态，在事务上操作（不提交）。
pub fn set_paper_fav_(tx: &Transaction, name: &str, fav: bool) -> Result<(), Error> {
    let ifav = match fav {
        false => 0,
        true => 1,
    };
    tx.execute(
        "UPDATE papers SET fav = ? WHERE name = ?",
        params![ifav, name],
    )?;
    Ok(())
}

/// 通过文件名修改文献的收藏状态，在连接上操作（提交）。
pub fn set_paper_fav(conn: &Connection, name: &str, fav: bool) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    set_paper_fav_(&tx, name, fav)?;
    tx.commit()
}

/// 通过文献标识检查文献标识是否存在，在事务上操作。
fn exists_paperid_(tx: &Transaction, id: &str) -> Result<bool, Error> {
    let mut stmt = tx.prepare("SELECT * FROM papers WHERE id = ? LIMIT 1")?;
    stmt.exists([id])
}

/// 通过文献标识检查文献是否存在，在连接上操作。
fn exists_paperid(tx: &Transaction, id: &str) -> Result<bool, Error> {
    let mut stmt = tx.prepare("SELECT * FROM papers WHERE id = ? LIMIT 1")?;
    stmt.exists([id])
}

/// 通过文献名检查文献是否存在，在事务上操作。
pub fn exists_paper_(tx: &Transaction, name: &str) -> Result<bool, Error> {
    let mut stmt = tx.prepare("SELECT * FROM papers WHERE name = ? LIMIT 1")?;
    stmt.exists([name])
}

/// 通过文献名检查文献是否存在，在连接上操作。
pub fn exists_paper(conn: &Connection, name: &str) -> Result<bool, Error> {
    let tx = conn.unchecked_transaction()?;
    exists_paper_(&tx, name)
}

/// 通过标识获取文献，在事务上操作。
fn _get_paper_(
    tx: &Transaction,
    id: &str,
) -> Result<
    Option<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let row = tx.query_row(
        "SELECT name, publisher_id, publisher_date, description, ctime, mtime, star, fav WHERE id = ?",
        [id],
        |row| {
            Ok((
                row.get::<_, String>(0)?,
                row.get::<_, Option<String>>(1)?,
                row.get::<_, Option<u64>>(2)?,
                row.get::<_, Option<String>>(3)?,
                row.get::<_, u64>(4)?,
                row.get::<_, u64>(5)?,
                row.get::<_, i8>(6)?,
                row.get::<_, i8>(7)?,
                ))
        }
    ).optional()?;

    match row {
        Some(v) => {
            let (name, publisherid, publisher_date, description, ctime, mtime, star, ifav) = v;
            let fav = match ifav {
                0 => false,
                _ => true,
            };
            Ok(Some((
                id.to_owned(),
                name,
                publisherid,
                publisher_date,
                description,
                ctime,
                mtime,
                star,
                fav,
            )))
        }
        None => Ok(None),
    }
}

/// 通过标识获取文献，在连接上操作。
fn _get_paper(
    conn: &Connection,
    id: &str,
) -> Result<
    Option<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let tx = conn.unchecked_transaction()?;
    _get_paper_(&tx, id)
}

/// 通过文件名获取文献，在事务上操作。
pub fn get_paper_(
    tx: &Transaction,
    name: &str,
) -> Result<
    Option<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let row = tx.query_row(
        "SELECT id, publisher_id, publisher_date, description, ctime, mtime, star, fav WHERE name = ?",
        [name],
        |row| {
            Ok((
                row.get::<_, String>(0)?,
                row.get::<_, Option<String>>(1)?,
                row.get::<_, Option<u64>>(2)?,
                row.get::<_, Option<String>>(3)?,
                row.get::<_, u64>(4)?,
                row.get::<_, u64>(5)?,
                row.get::<_, i8>(6)?,
                row.get::<_, i8>(7)?,
            ))
        }
    ).optional()?;

    match row {
        Some(v) => {
            let (id, publisherid, publisher_date, description, ctime, mtime, star, ifav) = v;
            let fav = match ifav {
                0 => false,
                _ => true,
            };
            Ok(Some((
                id,
                name.to_owned(),
                publisherid,
                publisher_date,
                description,
                ctime,
                mtime,
                star,
                fav,
            )))
        }
        None => Ok(None),
    }
}

/// 通过文件名获取文献，在连接上操作。
pub fn get_paper(
    conn: &Connection,
    name: &str,
) -> Result<
    Option<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let tx = conn.unchecked_transaction()?;
    get_paper_(&tx, name)
}

/// 通过部分名获取文献，在事务上操作。
pub fn get_papers_with_namepart_(
    tx: &Transaction,
    name_part: &str,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let name_pat = pat(name_part);
    let mut stmt = tx.prepare(
        "SELECT id, name,publisher_id, publisher_date, description, ctime, mtime, star, fav WHERE name LIKE ?"
    )?;

    let rows = stmt.query_map([name_pat], |row| {
        Ok((
            row.get::<_, String>(0)?,
            row.get::<_, String>(1)?,
            row.get::<_, Option<String>>(2)?,
            row.get::<_, Option<u64>>(3)?,
            row.get::<_, Option<String>>(4)?,
            row.get::<_, u64>(5)?,
            row.get::<_, u64>(6)?,
            row.get::<_, i8>(7)?,
            row.get::<_, i8>(8)?,
        ))
    })?;

    let mut papers: Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )> = Vec::new();
    for row in rows {
        let (id, name, publisherid, publisher_date, description, ctime, mtime, star, ifav) = row?;
        let fav = match ifav {
            0 => false,
            _ => true,
        };
        papers.push((
            id,
            name,
            publisherid,
            publisher_date,
            description,
            ctime,
            mtime,
            star,
            fav,
        ))
    }
    Ok(papers)
}

/// 通过部分名获取文献，在连接上操作。
pub fn get_papers_with_namepart(
    conn: &Connection,
    name_part: &str,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let tx = conn.unchecked_transaction()?;
    get_papers_with_namepart_(&tx, name_part)
}

/// 根据收藏状态获取文献，在事务上操作。
pub fn get_papers_with_fav_(
    tx: &Transaction,
    fav: bool,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let ifav: i8 = match fav {
        false => 0,
        true => 1,
    };

    let mut stmt = tx.prepare(
        "SELECT id, name, publisher_id, publisher_date, description, ctime, mtime star WHERE fav = ?"
    )?;

    let rows = stmt.query_map([ifav], |row| {
        Ok((
            row.get::<_, String>(0)?,
            row.get::<_, String>(1)?,
            row.get::<_, Option<String>>(2)?,
            row.get::<_, Option<u64>>(3)?,
            row.get::<_, Option<String>>(4)?,
            row.get::<_, u64>(5)?,
            row.get::<_, u64>(6)?,
            row.get::<_, i8>(7)?,
        ))
    })?;

    let mut papers: Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )> = Vec::new();
    for row in rows {
        let (id, name, publisherid, publish_date, description, ctime, mtime, star) = row?;
        papers.push((
            id,
            name,
            publisherid,
            publish_date,
            description,
            ctime,
            mtime,
            star,
            fav,
        ));
    }
    Ok(papers)
}

/// 根据收藏状态获取文献，在连接上操作。
pub fn get_papers_with_fav(
    conn: &Connection,
    fav: bool,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let tx = conn.unchecked_transaction()?;
    get_papers_with_fav_(&tx, fav)
}

/// 通过出版社标识获取文献，在事务上操作。
fn get_papers_with_publisherid_(
    tx: &Transaction,
    publisherid: &str,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let mut stmt = tx.prepare("SELECT id, name, publisher_date, description, ctime, mtime, star, fav FROM papers WHERE publisher_id = ?")?;
    let rows = stmt.query_map([publisherid], |row| {
        Ok((
            row.get(0)?,
            row.get(1)?,
            row.get(2)?,
            row.get(3)?,
            row.get(4)?,
            row.get(5)?,
            row.get(6)?,
            row.get(7)?,
        ))
    })?;
    let mut papers: Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )> = Vec::new();
    for row in rows {
        let (id, name, publisher_date, description, ctime, mtime, star, ifav) = row?;
        let fav = match ifav {
            0 => false,
            _ => true,
        };
        papers.push((
            id,
            name,
            Some(publisherid.to_owned()),
            publisher_date,
            description,
            ctime,
            mtime,
            star,
            fav,
        ));
    }
    Ok(papers)
}

/// 通过出版社标识获取文献，在连接上操作。
fn get_papers_with_publisherid(
    conn: &Connection,
    publisherid: &str,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let tx = conn.unchecked_transaction()?;
    get_papers_with_publisherid_(&tx, publisherid)
}

/// 通过出版社（期刊、会议）获取文献，在事务上操作（不提交）。
pub fn get_papers_with_publisher_(
    tx: &Transaction,
    publisher: &str,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>, Error, > {
    let publisherid = get_publisher_(tx, publisher)?.unwrap().0;
    get_papers_with_publisherid_(tx, &publisherid)
}

/// 通过出版社（期刊、会议）获取文献，在连接上操作（提交）。
pub fn get_papers_with_publisher(
    conn: &Connection,
    publisher: &str,
) -> Result<
    Vec<(
        String,
        String,
        Option<String>,
        Option<u64>,
        Option<String>,
        u64,
        u64,
        i8,
        bool,
    )>,
    Error,
> {
    let tx = conn.unchecked_transaction()?;
    get_papers_with_publisher_(&tx, publisher)
}

/// 添加作者标识-标签标识对，在事务上操作（不提交）。
fn add_authorid_tagid_(tx: &Transaction, authorid: &str, tagid: &str) -> Result<(), Error> {
    let ctime = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    tx.execute(
        "INSERT INTO author_tag (author_id, tag_id, ctime) VALUES (?, ?, ?)",
        params![authorid, tagid, ctime],
    )?;
    Ok(())
}

/// 添加作者标识-标签标识对，在连接上操作（提交）。
fn add_authorid_tagid(conn: &Connection, authorid: &str, tagid: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_authorid_tagid_(&tx, authorid, tagid)?;
    tx.commit()
}

/// 添加作者名-标签名对，在事务上操作（不提交）。
pub fn add_author_tag_(
    tx: &Transaction,
    author_name: &str,
    tag_name: &str,
) -> Result<(), Error> {
    let authorid = _get_author_(tx, author_name)?.unwrap().0;
    let tagid = get_tag_(tx, tag_name)?.unwrap().0;
    add_authorid_tagid_(tx, &authorid, &tagid)
}

/// 添加作者名-标签名对，在连接上操作（提交）。
pub fn add_author_tag(conn: &Connection, author_name: &str, tag_name: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_author_tag_(&tx, author_name, tag_name)?;
    tx.commit()
}

/// 删除作者标识-标签标识对（不提交）。
fn rm_authorid_tagid_(tx: &Transaction, authorid: &str, tagid: &str) -> Result<(), Error> {
    tx.execute(
        "DELETE FROM author_tag WHERE author_id = ? AND tag_id = ?",
        params![authorid, tagid],
    )?;
    Ok(())
}

/// 利用id删除作者-标签对（提交）。
fn rm_authorid_tagid(conn: &Connection, author_id: &str, tagid: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_authorid_tagid_(&tx, author_id, tagid)?;
    tx.commit()
}

/// 移除作者-标签链接，但不提交。
pub fn rm_author_tag_(tx: &Transaction, author_name: &str, tag_name: &str) -> Result<(), Error> {
    let author_id = get_author_(tx, author_name)?.unwrap().0;
    let tagid = get_tag_(tx, tag_name)?.unwrap().0;
    rm_authorid_tagid_(tx, &author_id, &tagid)
}

/// 移除作者-标签（提交）。
pub fn rm_author_tag(conn: &Connection, author: &str, tag: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_author_tag_(&tx, author, tag)?;
    tx.commit()
}

/// 移除给定作者的所有标签。
pub fn rm_author_tags(conn: &Connection, author: &str) -> Result<(), Error> {
    let author_id = get_author(conn, author)?.unwrap().1;
    conn.execute("DELETE FROM author_tag WHERE author_id = ?", [author_id])?;
    Ok(())
}

/// 为作者标识添加标签标识，在事务上操作（不提交）。
///
/// 若作者标识-标签标识对已存在，则返回Err。
fn attach_tagid_to_authorid_(
    tx: &Transaction,
    authorid: &str,
    tagid: &str,
) -> Result<(), Error> {
    let ctime = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
    tx.execute("INSERT INTO author_tag VALUES (?, ?, ?)", params![authorid, tagid, ctime])?;
    Ok(())
}

/// 设置作者标识的标签，在连接上操作（提交）。
fn attach_tagid_to_authorid(conn: &Connection, authorid: &str, tagid: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    attach_tagid_to_authorid_(&tx, authorid, tagid)?;
    tx.commit()
}

/// 设置作者名的标签，在事务上操作（不提交）。
pub fn attach_tag_to_author_(
    tx: &Transaction,
    author: &str,
    tag: &str,
) -> Result<(), Error> {
    let authorid = get_author_(tx, author)?.unwrap().0;
    let tagid = get_tag_(tx, tag)?.unwrap().0;
    attach_tagid_to_authorid_(tx, &authorid, &tagid)
}
/// 设置作者的标签，在连接上操作（提交）。
pub fn attach_tag_to_author(conn: &Connection, author: &str, tag: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    attach_tag_to_author_(&tx, author, tag)?;
    tx.commit()
}

/// 检查作者标识-标签标识是否存在，在事务上操作。
fn exists_authorid_tagid_(
    tx: &Transaction,
    author_id: &str,
    tagid: &str,
) -> Result<bool, Error> {
    let mut stmt = tx.prepare("SELECT * FROM author_tag WHERE author_id = ? AND tag_id = ?")?;
    stmt.exists(params![author_id, tagid])
}

/// 检查作者标识-标签标识是否存在，在连接上操作。
fn exists_authorid_tagid(
    conn: &Connection,
    author_id: &str,
    tagid: &str,
) -> Result<bool, Error> {
    let mut stmt = conn.prepare("SELECT * FROM author_tag WHERE author_id = ? AND tag_id = ?")?;
    stmt.exists(params![author_id, tagid])
}

/// 检查作者名-标签名是否存在，在事务上操作。
pub fn exists_author_tag_(
    tx: &Transaction,
    author_name: &str,
    tag_name: &str,
) -> Result<bool, Error> {
    if !(exists_author_(tx, author_name)? && exists_tag_(tx, tag_name)?) {
        return Ok(false);
    }

    let author_id = get_author_(tx, tag_name)?.unwrap().0;
    let tagid = get_tag_(tx, tag_name)?.unwrap().0;
    let mut stmt = tx.prepare("SELECT * FROM author_tag WHERE author_id = ? AND tag_id = ?")?;
    stmt.exists(params![author_id, tagid])
}

/// 检查作者名-标签名是否存在，在连接上操作。
pub fn exists_author_tag(
    conn: &Connection,
    author_name: &str,
    tag_name: &str,
) -> Result<bool, Error> {
    if !(exists_author(conn, author_name)? && exists_tag(conn, tag_name)?) {
        return Ok(false);
    }

    let author_id = get_author(conn, tag_name)?.unwrap().0;
    let tagid = get_tag(conn, tag_name)?.unwrap().0;
    let mut stmt = conn.prepare("SELECT * FROM author_tag WHERE author_id = ? AND tag_id = ?")?;
    stmt.exists(params![author_id, tagid])
}

/// 获取所给作者标识的全部标签标识，在事务上操作。
fn get_tagids_of_authorid_(tx: &Transaction, author_id: &str) -> Result<Vec<String>, Error> {
    let mut stmt = tx.prepare("SELECT tag_id FROM WHERE author_id = ?")?;
    let rows = stmt.query_map(params![author_id], |row| row.get::<_, String>(0))?;
    let mut tagids = Vec::new();
    for row in rows {
        tagids.push(row?);
    }
    Ok(tagids)
}

/// 获取所给作者标识的所有标签标识，在连接上操作。
fn get_tagids_of_authorid(conn: &Connection, author_id: &str) -> Result<Vec<String>, Error> {
    let mut stmt = conn.prepare("SELECT tag_id FROM WHERE author_id = ?")?;
    let rows = stmt.query_map(params![author_id], |row| row.get::<_, String>(0))?;
    let mut tagids = Vec::new();
    for row in rows {
        tagids.push(row?);
    }
    Ok(tagids)
}

/// 获取给定作者名的所有标签。
pub fn get_tags_of_author_(
    tx: &Transaction,
    author: &str,
) -> Result<Vec<(String, String, bool)>, Error> {
    let author_id = get_author_(tx, author).unwrap().unwrap().0;
    let tagids = get_tagids_of_authorid_(tx, &author_id)?;
    let mut tags = Vec::new();
    for tagid in tagids {
        let tag = _get_tag_(tx, &tagid)?.unwrap();
        tags.push(tag);
    }
    Ok(tags)
}

/// 获取给定作者名的所有标签。
pub fn get_tags_of_author(
    conn: &Connection,
    author: &str,
) -> Result<Vec<(String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tags_of_author_(&tx, author)
}

/// 获取具有所给标签标识的作者标识，在事务上操作。
fn get_authorids_with_tagid_(tx: &Transaction, tagid: &str) -> Result<Vec<String>, Error> {
    let mut stmt = tx.prepare("SELECT author_id FROM author_tag WHERE tag_id = ?")?;
    let rows = stmt.query_map([tagid], |row| row.get::<_, String>(0))?;
    let mut author_ids = Vec::new();
    for row in rows {
        author_ids.push(row?);
    }
    Ok(author_ids)
}

/// 获取具有所给标签标识的作者标识，在连接上操作。
fn get_authorids_with_tagid(conn: &Connection, tagid: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_authorids_with_tagid_(&tx, tagid)
}

/// 获取具有指定标签名的作者，在事务上操作。
pub fn get_authors_with_tag_(
    tx: &Transaction,
    tag: &str,
) -> Result<Vec<(String, String, String, String, bool)>, Error> {
    let tagid = get_tag(tx, tag)?.unwrap().0;
    let authorids = get_authorids_with_tagid_(tx, &tagid)?;
    let mut authors = Vec::new();
    for authorid in authorids.iter() {
        let author = _get_author_(tx, authorid)?.unwrap();
        authors.push(author);
    }
    Ok(authors)
}

/// 获取具有指定标签标识的作者，在连接上操作。
pub fn get_authors_with_tag(conn: &Connection, tag: &str) -> Result<Vec<(String, String, String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_authors_with_tag_(&tx, tag)
}

//********************************************************************
//******************* paper-author 表操纵 ****************************
//********************************************************************

/// 添加出版社标识-标签标识对，在事务上操作（不提交）。
fn add_publisherid_tagid_(
    tx: &Transaction,
    publisherid: &str,
    tagid: &str,
) -> Result<(), Error> {
    let ctime = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    tx.execute(
        "INSERT INTO publisher_tag VALUES (?, ?, ?)",
        params![publisherid, tagid, ctime],
    )?;
    Ok(())
}

/// 添加出版社标识-标签标识对，在连接上操作（提交）。
fn add_publisherid_tagid(
    conn: &Connection,
    publisherid: &str,
    tagid: &str,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_publisherid_tagid_(&tx, publisherid, tagid)?;
    tx.commit()
}

/// 添加出版社-标签对，在事务上操作（不提交）。
pub fn add_publisher_tag_(tx: &Transaction, publisher: &str, tag: &str) -> Result<(), Error> {
    let publisherid = get_publisher_(tx, publisher)?.unwrap().0;
    let tagid = get_tag_(tx, tag)?.unwrap().0;
    add_publisherid_tagid_(tx, &publisherid, &tagid)
}

/// 添加出版社-标签对，在连接上操作（提交）。
pub fn add_publisher_tag(conn: &Connection, publisher: &str, tag: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_publisher_tag_(&tx, publisher, tag)?;
    tx.commit()
}

/// 删除出版社标识-标签标识对，在事务上操作（不提交）。
fn rm_publisherid_tagid_(
    tx: &Transaction,
    publisherid: &str,
    tagid: &str,
) -> Result<(), Error> {
    tx.execute(
        "DELETE FROM publisher_tag WHERE publisher_id = ? AND tag_id = ?",
        params![publisherid, tagid],
    )?;
    Ok(())
}

/// 删除出版社标识-标签标识对，在连接上操作（提交）。
fn rm_publisherid_tagid(
    conn: &Connection,
    publisherid: &str,
    tagid: &str,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_publisherid_tagid_(&tx, publisherid, tagid)?;
    tx.commit()
}

/// 删除出版社-标签对，在事务上操作（不提交）。
pub fn rm_publisher_tag_(tx: &Transaction, publisher: &str, tag: &str) -> Result<(), Error> {
    let publisherid = get_publisher_(tx, publisher)?.unwrap().0;
    let tagid = get_tag_(tx, tag)?.unwrap().0;
    tx.execute(
        "DELETE FROM publisher_tag WHERE publisher_id = ? AND tag_id = ?",
        params![publisherid, tagid],
    )?;
    Ok(())
}

/// 删除出版社-标签对，在连接上操作（提交）。
pub fn rm_publisher_tag(conn: &Connection, publisher: &str, tag: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_publisher_tag_(&tx, publisher, tag)?;
    tx.commit()
}

/// 设置出版社标识的标签标识，在事务上操作（不提交）。
fn attach_tagid_to_publisherid_(
    tx: &Transaction,
    publisherid: &str,
    tagid: &str,
) -> Result<(), Error> {
    let ctime = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
    tx.execute("INSERT INTO publisher_tag VALUES (?, ?, ?)", params![publisherid, tagid, ctime])?;
    Ok(())
}

/// 设置出版社标识的标签标识，在连接上操作（提交）。
fn attach_tagid_to_publisherid(
    conn: &Connection,
    publisherid: &str,
    tagid: &str,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    attach_tagid_to_publisherid_(&tx, publisherid, tagid)?;
    tx.commit()
}

/// 设置出版社标签，在事务上操作（不提交）。
pub fn attach_tag_to_publisher_(
    tx: &Transaction,
    publisher: &str,
    tag: &str,
) -> Result<(), Error> {
    let publisherid = get_publisher_(tx, publisher)?.unwrap().0;
    let tagid = get_tag_(tx, tag)?.unwrap().0;
    attach_tagid_to_publisherid_(tx, &publisherid, &tagid)?;
    Ok(())
}

/// 设置出版社标签（提交）。
pub fn attach_tag_to_publisher(
    conn: &Connection,
    name: &str,
    tag: &str,
) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    attach_tag_to_publisher_(&tx, name, tag)?;
    tx.commit()
}

/// 根据出版社标识获取标签标识。
fn get_tagids_of_publisherid_(
    tx: &Transaction,
    publisherid: &str,
) -> Result<Vec<String>, Error> {
    let mut stmt = tx.prepare("SELECT tag_id FROM publisher_tag WHERE publisher_id = ?")?;
    let rows = stmt.query_map([publisherid], |row| row.get(0))?;
    let mut tagids = Vec::new();
    for row in rows {
        tagids.push(row?);
    }
    Ok(tagids)
}

/// 根据出版社标识获取标签标识。
fn get_tagids_of_publisherid(
    conn: &Connection,
    publisherid: &str,
) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tagids_of_publisherid_(&tx, publisherid)
}

/// 根据出版社名字获取标签。
pub fn get_tags_of_publisher_(
    tx: &Transaction,
    name: &str,
) -> Result<Vec<(String, String, bool)>, Error> {
    let publisherid = get_tag_(tx, name)?.unwrap().0;
    let tagids = get_tagids_of_publisherid_(tx, &publisherid)?;
    let mut tags = Vec::new();
    for tagid in tagids {
        let tag = _get_tag_(tx, &tagid)?.unwrap();
        tags.push(tag);
    }
    Ok(tags)
}

/// 根据出版社名字获取标签。
pub fn get_tags_of_publisher(
    conn: &Connection,
    name: &str,
) -> Result<Vec<(String, String, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tags_of_publisher_(&tx, name)
}

/// 获取具有指定标签标识的出版社标识。
fn get_publisherids_with_tagid_(tx: &Transaction, tagid: &str) -> Result<Vec<String>, Error> {
    let mut stmt = tx.prepare("SELECT publisher_id WHERE tag_id = ?")?;
    let rows = stmt.query_map(params![tagid], |row| row.get(0))?;
    let mut publisherids: Vec<String> = Vec::new();
    for row in rows {
        publisherids.push(row?);
    }
    Ok(publisherids)
}

/// 获取具有指定标签标识的出版社标识，在连接上操作。
fn get_publisherids_with_tagid(conn: &Connection, tagid: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_publisherids_with_tagid_(&tx, tagid)
}

/// 根据标签名获取具有指定标签的出版社，在事务上操作。
pub fn get_publishers_with_tag_(
    tx: &Transaction,
    tag: &str,
) -> Result<Vec<(String, String, Option<f32>, Option<String>, bool)>, Error> {
    let tagid = get_tag_(tx, tag)?.unwrap().0;
    let publisherids = get_publisherids_with_tagid_(tx, &tagid)?;
    let mut publishers = Vec::new();
    for publisherid in publisherids.iter() {
        publishers.push(_get_publisher_(tx, publisherid)?.unwrap())
    }
    Ok(publishers)
}

/// 根据标签名获取具有指定标签的出版社。
pub fn get_publishers_with_tag(
    conn: &Connection,
    tag: &str,
) -> Result<Vec<(String, String, Option<f32>, Option<String>, bool)>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_publishers_with_tag_(&tx, tag)
}

//****************************************************************************
//********************** Paper-Tag 表操控 *************************************
//****************************************************************************

/// 添加文献标识-标签标识对，在事务上操作（不提交）。
fn add_paperid_tagid_(tx: &Transaction, paperid: &str, tagid: &str) -> Result<(), Error> {
    let ctime = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    tx.execute(
        "INSERT INTO paper_tag VALUES (?, ?, ?)",
        params![paperid, tagid, ctime],
    )?;
    Ok(())
}

/// 添加文献标识-标签标识对，在连接上操作（提交）。
fn add_paperid_tagid(conn: &Connection, paperid: &str, tagid: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_paperid_tagid_(&tx, paperid, tagid)?;
    tx.commit()
}

/// 添加文献-标签对，在事务上操作（不提交）。
pub fn add_paper_tag_(tx: &Transaction, paper: &str, tag: &str) -> Result<(), Error> {
    let paper_id = get_paper_(tx, paper)?.unwrap().0;
    let tagid = get_tag_(tx, tag)?.unwrap().0;
    let ctime = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    tx.execute(
        "INSERT INTO paper_tag VALUES (?, ?, ?)",
        params![paper_id, tagid, ctime],
    )?;
    Ok(())
}

/// 添加文献-标签对，在连接上操作（提交）。
pub fn add_paper_tag(conn: &Connection, paper: &str, tag: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_paper_tag_(&tx, paper, tag)?;
    tx.commit()
}

/// 删除文献标识-标签标识对，在事务上操作（不提交）。
fn rm_paperid_tagid_(tx: &Transaction, paper_id: &str, tagid: &str) -> Result<(), Error> {
    tx.execute(
        "DELETE FROM paper_tag WHERE paper_id = ? AND tag_id = ?",
        params![paper_id, tagid],
    )?;
    Ok(())
}

/// 删除文献标识-标签标识对（提交）。
fn rm_paperid_tagid(conn: &Connection, paperid: &str, tagid: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_paperid_tagid_(&tx, paperid, tagid)?;
    tx.commit()
}

/// 删除文献-标签对（不提交）。
pub fn rm_paper_tag_(tx: &Transaction, paper: &str, tag: &str) -> Result<(), Error> {
    let paper_id = get_paper_(tx, paper)?.unwrap().0;
    let tagid = get_tag_(tx, tag)?.unwrap().0;
    rm_paperid_tagid_(tx, &paper_id, &tagid)?;
    Ok(())
}

/// 删除文献标识-标签标识对（提交）。
pub fn rm_paper_tag(conn: &Connection, paper: &str, tag: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    rm_paper_tag_(&tx, paper, tag)?;
    tx.commit()
}

/// 获取所给文献标识的标签标识，在事务上操作。
fn get_tagids_of_paperid_(tx: &Transaction, paperid: &str) -> Result<Vec<String>, Error> {
    let mut stmt = tx.prepare("SELECT tag_id FROM paper_tag WHERE paper_id = ?")?;
    let rows = stmt.query_map(params![paperid], |row| row.get(0))?;
    let mut tagids = Vec::new();
    for row in rows {
        tagids.push(row?);
    }
    Ok(tagids)
}

/// 获取所给文献标识的标签标识，在连接上操作。
fn get_tagids_of_paperid(conn: &Connection, paperid: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tagids_of_paperid_(&tx, paperid)
}

/// 获取所给文献的标签，在事务上操作。
pub fn get_tags_of_paper_(tx: &Transaction, paper: &str) -> Result<Vec<String>, Error> {
    let paper_id = get_paper_(tx, paper)?.unwrap().0;
    let tagids = get_tagids_of_paperid_(tx, &paper_id)?;
    let mut tags = Vec::new();
    for tagid in tagids.iter() {
        tags.push(_get_tag_(tx, tagid).unwrap().unwrap().1);
    }
    Ok(tags)
}

/// 获取所给文献的标签，在事务上操作。
pub fn get_tags_of_paper(conn: &Connection, paper: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_tags_of_paper_(&tx, paper)
}

//********************************************************************
//******************* paper-author 表操纵 ****************************
//********************************************************************

/// 添加文献标识-作者标识对（不提交）。
fn add_paperid_authorid_(tx: &Transaction, paperid: &str, authorid: &str) -> Result<(), Error> {
    let ctime = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    tx.execute(
        "INSERT INTO paper_author VALUES (?, ?, ?)",
        params![paperid, authorid, ctime],
    )?;
    Ok(())
}

/// 添加文献标识-作者标识对（提交）。
fn add_paperid_authorid(conn: &Connection, paperid: &str, authorid: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_paperid_authorid_(&tx, paperid, authorid)?;
    tx.commit()
}

/// 添加文献-作者对（不提交）。
pub fn add_paper_author_(tx: &Transaction, paper: &str, author: &str) -> Result<(), Error> {
    let ctime = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    let paper_id = get_paper_(tx, paper)?.unwrap().0;
    let author_id = get_author_(tx, author)?.unwrap().0;
    tx.execute(
        "INSERT INTO paper_author VALUES (?, ?, ?)",
        params![paper_id, author_id, ctime],
    )?;
    Ok(())
}

/// 添加文献-作者对（提交）。
pub fn add_paper_author(conn: &Connection, paper: &str, author: &str) -> Result<(), Error> {
    let tx = conn.unchecked_transaction()?;
    add_paper_author_(&tx, paper, author)?;
    tx.commit()
}

/// 检查是否存在文献标识-作者标识对，在事务上操作。
fn exists_paperid_authorid_(
    tx: &Transaction,
    paper_id: &str,
    author_id: &str,
) -> Result<bool, Error> {
    let mut stmt = tx.prepare("SELECT * FROM paper_author WHERE paper_id = ? AND author_id = ?")?;
    stmt.exists([paper_id, author_id])
}

/// 检查是否存在文献标识-作者标识对，在连接上操作。
fn exists_paperid_authorid(
    conn: &Connection,
    paper_id: &str,
    author_id: &str,
) -> Result<bool, Error> {
    let tx = conn.unchecked_transaction()?;
    exists_paperid_authorid_(&tx, paper_id, author_id)
}

/// 检查是否存在文献-作者对，在事务上操作。
pub fn exists_paper_author_(tx: &Transaction, paper: &str, author: &str) -> Result<bool, Error> {
    let paper_id = get_paper_(tx, paper)?.unwrap().0;
    let author_id = get_author_(tx, author)?.unwrap().0;
    let mut stmt = tx.prepare("SELECT * FROM paper_author WHERE paper_id = ? AND author_id = ?")?;
    stmt.exists(params![paper_id, author_id])
}

/// 检查是否存在文献-作者对，在连接上操作。
pub fn exists_paper_author(conn: &Connection, paper: &str, author: &str) -> Result<bool, Error> {
    let tx = conn.unchecked_transaction()?;
    exists_paper_author_(&tx, paper, author)
}

/// 获取某文献标识的所有作者标识，在事务上操作。
fn get_authorids_of_paperid_(tx: &Transaction, paperid: &str) -> Result<Vec<String>, Error> {
    let mut stmt = tx.prepare("SELECT author_id FROM paper_author WHERE paper_id = ?")?;
    let rows = stmt.query_map([paperid], |row| row.get::<_, String>(0))?;
    let mut author_ids = Vec::new();
    for row in rows {
        author_ids.push(row?);
    }
    Ok(author_ids)
}

/// 获取某文献标识的所有作者标识，在连接上操作。
fn get_authorids_of_paperid(conn: &Connection, paperid: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_authorids_of_paperid_(&tx, paperid)
}

/// 获取某文献的所有作者，在事务上操作。
fn get_authors_of_paper_(tx: &Transaction, paper: &str) -> Result<Vec<String>, Error> {
    let paper_id = get_paper_(tx, paper)?.unwrap().0;
    let mut stmt = tx.prepare("SELECT author_id FROM paper_author WHERE paper_id = ?")?;
    let rows = stmt.query_map([&paper_id], |row| row.get::<_, String>(0))?;
    let mut authors = Vec::new();
    for row in rows {
        let author_id = row?;
        let author = _get_author_(tx, &author_id)?.unwrap().1;
        authors.push(author);
    }
    Ok(authors)
}

/// 获取某文献的所有作者，在连接上操作。
pub fn get_authors_of_paper(conn: &Connection, paper: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_authors_of_paper_(&tx, paper)
}

/// 获取某作者标识的所有文献标识，在事务上操作。
fn get_paperids_of_authorid_(tx: &Transaction, authorid: &str) -> Result<Vec<String>, Error> {
    let mut stmt = tx.prepare("SELECT paper_id FROM paper_author WHERE author_id = ?")?;
    let rows = stmt.query_map(params![authorid], |row| row.get::<_, String>(0))?;
    let mut paper_ids = Vec::new();
    for row in rows {
        let paper_id = row?;
        paper_ids.push(paper_id);
    }
    Ok(paper_ids)
}

/// 获取某作者标识的所有文献标识，在连接上操作。
fn get_paperids_of_authorid(conn: &Connection, author_id: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_paperids_of_authorid_(&tx, author_id)
}

/// 获取某作者的所有文献，在事务上操作。
pub fn get_papers_of_author_(tx: &Transaction, author: &str) -> Result<Vec<String>, Error> {
    let author_id = get_author_(tx, author)?.unwrap().0;
    let mut stmt = tx.prepare("SELECT paper_id FROM paper_author WHERE author_id = ?")?;
    let rows = stmt.query_map([&author_id], |row| row.get::<_, String>(0))?;
    let mut papers = Vec::new();
    for row in rows {
        let paper_id = row?;
        let paper = _get_paper_(tx, &paper_id)?.unwrap().1;
        papers.push(paper);
    }
    Ok(papers)
}

/// 获取某作者的所有文献，在连接上操作。
pub fn get_papers_of_author(conn: &Connection, author: &str) -> Result<Vec<String>, Error> {
    let tx = conn.unchecked_transaction()?;
    get_papers_of_author_(&tx, author)
}

#[cfg(test)]
mod test {
    use super::*;
    use std::path::Path;

    const DBPATH: &str = "/home/boz/Documents/资料/elib/profile.db";

    #[test]
    fn test_add_tags_() {
        let mut conn = open(&Path::new(DBPATH)).unwrap();
        clear_tags(&conn).unwrap();
        let tx = conn.transaction().unwrap();
        add_tag_(&tx, "ML", true).unwrap();
        add_tag_(&tx, "DL", false).unwrap();
        add_tag_(&tx, "NLP", true).unwrap();
        add_tag_(&tx, "REC", false).unwrap();
        tx.commit().unwrap();
    }

    #[test]
    fn test_add_tags() {
        let conn = open(&Path::new(DBPATH)).unwrap();
        clear_tags(&conn).unwrap();
        add_tag(&conn, "ML", true).unwrap();
        add_tag(&conn, "DL", false).unwrap();
    }

    #[test]
    fn test_rm_tag_with_name_() {
        let mut conn = open(&Path::new(DBPATH)).unwrap();
        clear_tags(&conn).unwrap();
        let tx = conn.transaction().unwrap();
        add_tag_(&tx, "ML", true).unwrap();
        add_tag_(&tx, "DL", false).unwrap();
        add_tag_(&tx, "NLP", true).unwrap();
        add_tag_(&tx, "REC", false).unwrap();
        rm_tag_(&tx, "ML").unwrap();
        tx.commit().unwrap();
    }
}
