use crate::dbop;
use regex;
use std::{path::Path, time::SystemTime};

#[derive(Debug)]
pub struct Tag {
    pub name: String,
    pub fav: bool,
}

#[derive(Debug)]
pub struct Author {
    pub name: String,
    pub contact: Option<String>,
    pub description: Option<String>,
    pub tags: Vec<String>,
    pub fav: bool,
}

#[derive(Debug)]
pub struct Publisher {
    pub name: String,
    pub impact: Option<f32>,
    pub description: Option<String>,
    pub tags: Vec<String>,
    pub fav: bool,
}

#[derive(Debug)]
pub struct Paper {
    pub name: String,
    pub authors: Vec<String>,
    pub publisher: Option<String>,
    pub publish_date: Option<u64>,
    pub description: Option<String>,
    pub tags: Vec<String>,
    pub ctime: u64,
    pub mtime: u64,
    pub fav: bool,
}
