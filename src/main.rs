pub mod dbop;
pub mod libop;

pub enum Error {
    DBOperationError((i32, String)),
    FSOperationError((i32, String)),
}

fn main() {}